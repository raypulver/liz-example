'use strict';

const fs = require('fs-extra');
const path = require('path');
const dir = path.join(process.env.HOME, '.liz-example');
const filename = path.join(dir, 'name.json');
const {promisify} = require('bluebird');
const _ = require('lodash');
const prettyStringify = _.partialRight(_.bindKey(JSON, 'stringify'), null, 1);
const writeToFile = _.partial(_.bindKey(fs, 'writeFile'), filename);
const {flow, zipObject} = require('lodash/fp');
const zipToName = zipObject(['name']);
const saveNameImpl = flow(_.castArray, zipToName, prettyStringify, writeToFile);
const saveName = async (...args) => {
  await mkNamePath();
  return saveNameImpl(...args);
};
const rimraf = promisify(require('rimraf'));
const filterName = flow(_.method('replace', /s+/g, ''), _.method('trim'));
const loadName = async name => {
  try {
    return JSON.parse(await fs.readFile(path.join(dir, 'name.json'), 'utf8')).name;
  } catch (e) {
    console.error(e.stack);
    return '<unknown>';
  }
};

const mkNamePath = _.partial(_.bindKey(fs, 'mkdirp'), dir);

const nameExists = _.partial(_.bindKey(fs, 'pathExists'), filename);

Object.assign(module.exports, {
  saveName,
  loadName,
  filterName,
  nameExists,
  mkNamePath
});
