'use strict';

const readlineSync = require('readline-sync');
const _ = require('lodash');
const question = _.bindKey(readlineSync, 'question');
const util = require('util');
const name = require('./name');
module.exports = async () => {
  let username;
  if (await name.nameExists()) {
    username = await name.loadName();
    console.log('welcome back ' + username);
  } else {
    const username = question('what\'s your name?\n');
    console.log('nice to meet you ' + username);
    await name.saveName(username);
  }
};
